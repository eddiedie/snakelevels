// Fill out your copyright notice in the Description page of Project Settings.


#include "Counter.h"
#include "Components/TextBlock.h"
#include "MyGameInstance.h"
#include "MyPawn.h"
#include "Kismet/GameplayStatics.h"


bool UCounter::Initialize()
{
	Super::Initialize();
	UpdateTextHit();
	UpdateTextLives();
	UpdateTextLevels();
	UpdateTextFoodDeaths();
	UpdateTextFoodNumCounter();
	//FTimerHandle InputTimeHandle;
	//GetWorld()->GetTimerManager().SetTimer(InputTimeHandle, this, &UCounter::UpdateTextHit, 1, false, 2);

	return true;
}


void UCounter::UpdateTextHit()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		CountHit = MyGameInstance->Hits;

		TextHitCounter->SetText(FText::FromString(FString::FromInt(CountHit)));
		//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Cyan, FString::FromInt(MyGameInstance->Hits));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Count Hits: %d"), MyGameInstance->Hits));
	}
}

void UCounter::UpdateTextLives()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		CountLives = MyGameInstance->Lives;

		TextLivesCounter->SetText(FText::FromString(FString::FromInt(CountLives)));
		//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Cyan, FString::FromInt(MyGameInstance->Lives));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Count Lives: %d"), MyGameInstance->Lives));
	}
}

void UCounter::UpdateTextLevels()
{
	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level1st"))
	{
		TextLevels->SetText(FText::FromString("Level 1"));
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level2nd"))
	{
		TextLevels->SetText(FText::FromString("Level 2"));
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level3rd"))
	{
		TextLevels->SetText(FText::FromString("Level 3"));
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level4th"))
	{
		TextLevels->SetText(FText::FromString("Level 4"));
	}
}

void UCounter::UpdateTextFoodDeaths()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		CountFoodDeaths = MyGameInstance->FoodDeaths;

		TextFoodDeathCounter->SetText(FText::FromString(FString::FromInt(CountFoodDeaths)));
		//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Cyan, FString::FromInt(MyGameInstance->FoodDeaths));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Food Deaths: %d"), MyGameInstance->FoodDeaths));
	}
}

void UCounter::UpdateTextFoodNumCounter()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		CountFoodNum = MyGameInstance->FoodNumber;

		TextFoodNumCounter->SetText(FText::FromString(FString::FromInt(CountFoodNum)));
		//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Cyan, FString::FromInt(MyGameInstance->FoodNumber));
	}
}

