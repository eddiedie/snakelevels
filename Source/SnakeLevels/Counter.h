// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Counter.generated.h"

class UTextBlock;

UCLASS()
class SNAKELEVELS_API UCounter : public UUserWidget
{
	GENERATED_BODY()
public:



	virtual bool Initialize();

public:

	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextHitCounter;

	int32 CountHit = 0;


	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextLivesCounter;

	int32 CountLives = 0;


	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextLevels;


	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextFoodDeathCounter;

	int32 CountFoodDeaths;

	
		UPROPERTY(meta = (BindWidget))
		UTextBlock* TextFoodNumCounter;

	int32 CountFoodNum;




public:
	UFUNCTION()
		void UpdateTextHit();

	UFUNCTION()
		void UpdateTextLives();

	UFUNCTION()
		void UpdateTextLevels();

	UFUNCTION()
		void UpdateTextFoodDeaths();

	UFUNCTION()
		void UpdateTextFoodNumCounter();
	
};
