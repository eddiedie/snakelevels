// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

#include "FoodDeath.h"
#include "MyGameInstance.h"

#include "SnakeBase1st.h"
#include "MyPawn.h"
#include "Components/SphereComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("BoxCollision"));
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	//SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AFood::OnOverlapBegin);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFood::SetFoodRed_Implementation()
{

}

void AFood::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase1st>(Interactor);
		if (Snake)
		{
			
			
			if (this->ActorHasTag(FName("FoodMoved"))/*this->GetActorLabel() == "FoodMoved"*/)
			{
				//this->SetActorLabel("Destroyed");
				this->Tags.Add(FName("Destroyed"));
				this->Destroy();
				

				//Snake->RemoveDestroyed(this);
				Snake->GrowSnakeOnFood();

				/*if (Snake->FoodArray.Num() <= 0)
				{
					auto MyPawn = Cast<AMyPawn>(GetWorld()->GetFirstPlayerController()->GetPawn());
					if (IsValid(MyPawn))
					{
						MyPawn->ShowWinMenu();
					}
				}*/

			}
			if (this->ActorHasTag(FName("SetSpeed")) /*this->GetActorLabel() == "SetSpeed"*/)
			{
				//this->SetActorLabel("Destroyed");
				this->Tags.Add(FName("Destroyed"));
				this->Destroy();
				
				Snake->GrowSnakeOnFood();

				/*if (Snake->FoodArray.Num() <= 0)
				{
					auto MyPawn = Cast<AMyPawn>(GetWorld()->GetFirstPlayerController()->GetPawn());
					if (IsValid(MyPawn))
					{
						MyPawn->ShowWinMenu();
					}
				}*/

				if (Snake->SpeedChange == false)
				{
					Snake->SnakeSpeed *= Snake->SpeedUP;
					Snake->SpeedChange = true;
				}
				else
				{
					Snake->SnakeSpeed /= Snake->SpeedUP;
					Snake->SpeedChange = false;
				}
				
			}
		}
		UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		if (IsValid(MyGameInstance))
		{
			MyGameInstance->Hits++;
			MyGameInstance->UpdateHits();

			MyGameInstance->FoodNumber--;
			MyGameInstance->UpdateFoodNumber();

			
		}
	}
}







void AFood::FoodInteraction(AActor* Interactor)
{
	AFoodDeath* FoodDeath = Cast<AFoodDeath>(Interactor);
	
	if (IsValid(FoodDeath))
	{
		//this->SetActorLabel("Destroyed");
		this->Tags.Add(FName("Destroyed"));
		this->Destroy();
		
		UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		if (IsValid(MyGameInstance))
		{

			GEngine->AddOnScreenDebugMessage(3, 5, FColor::Red, "MyGameInstance Casted!");

			MyGameInstance->FoodDeaths--;
			MyGameInstance->UpdateFoodDeaths();

			MyGameInstance->FoodNumber--;
			MyGameInstance->UpdateFoodNumber();
			
			if (MyGameInstance->FoodDeaths <= 0)
			{
				MyGameInstance->Lives--;
				MyGameInstance->UpdateLives();
				MyGameInstance->ShowGameOverMenu();
			}
		}
		
	}

	
}
