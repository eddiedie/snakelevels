// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SnakeInterface.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"


class ASnakeBase1st;
class USphereComponent;


UCLASS()
class SNAKELEVELS_API AFood : public AActor, public ISnakeInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SphereComponent")
		USphereComponent* SphereComponent;

	UPROPERTY()
		ASnakeBase1st* FoodOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetFoodRed();
		void SetFoodRed_Implementation();


		virtual void SnakeInteraction(AActor* Interactor, bool bIsHead) override;

	

		virtual void FoodInteraction(AActor* Interactor) override;

};
