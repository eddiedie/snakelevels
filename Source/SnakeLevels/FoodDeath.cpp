// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodDeath.h"
#include "MyGameInstance.h"
#include "Food.h"
#include "SnakeBase1st.h"
#include "Components/BoxComponent.h"

// Sets default values
AFoodDeath::AFoodDeath()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
}

// Called when the game starts or when spawned
void AFoodDeath::BeginPlay()
{
	Super::BeginPlay();
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AFoodDeath::OnOverlapBegin);
}

// Called every frame
void AFoodDeath::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}




void AFoodDeath::OnOverlapBegin
(
	UPrimitiveComponent*	OverlappedComp,
	AActor*					OtherActor,
	UPrimitiveComponent*	OtherComponent,
	int32					OtherBodyIndex,
	bool					bFromSweep,
	const					FHitResult& SweepResult
)
{
	
	AFood* Food = Cast<AFood>(OtherActor);

	if (IsValid(Food))
	{
		Food->FoodInteraction(this);
	}
	
}




void AFoodDeath::FoodInteraction(AActor* Interactor)
{
	if (Interactor)
	{
		AFood* Food = Cast<AFood>(Interactor);
		if (IsValid(Food))
		{
			GEngine->AddOnScreenDebugMessage(3, 5, FColor::Red, "Food Casted!");

			
		}
	}
}
