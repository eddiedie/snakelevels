// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SnakeInterface.h"
#include "GameFramework/Actor.h"
#include "FoodDeath.generated.h"

class ASnakeBase1st;
class UBoxComponent;

UCLASS()
class SNAKELEVELS_API AFoodDeath : public AActor, public ISnakeInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodDeath();


	UPROPERTY()
		ASnakeBase1st* FoodDeathOwner;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SphereComponent")
		UBoxComponent* BoxComponent;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;






	UFUNCTION()
		void OnOverlapBegin
		(
			UPrimitiveComponent*	OverlappedComp,
			AActor*					OtherActor,
			UPrimitiveComponent*	OtherComponent,
			int32					OtherBodyIndex,
			bool					bFromSweep,
			const					FHitResult& SweepResult
		);


	
	virtual void FoodInteraction(AActor* Interactor) override;
};
