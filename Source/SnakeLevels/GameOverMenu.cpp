// Fill out your copyright notice in the Description page of Project Settings.


#include "GameOverMenu.h"
#include "Components/Button.h"
#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"

bool UGameOverMenu::Initialize()
{
	Super::Initialize();

	BtnStartAgain->OnClicked.AddDynamic(this, &UGameOverMenu::StartAgainClicked);
	BtnQuitToMain->OnClicked.AddDynamic(this, &UGameOverMenu::QuitToMainClicked);

	return true;
}

void UGameOverMenu::StartAgainClicked()
{
	UE_LOG(LogTemp, Warning, TEXT("Our button is working!"));

	/*Get reference to PlayerController*/
	APlayerController* PLayerController = GetWorld()->GetGameInstance()->GetFirstLocalPlayerController();

	/*Set Up input parameters to the SetInputMode function*/
	FInputModeGameOnly InputModeData;
	//InputModeData.SetWidgetToFocus(MainMenu->TakeWidget());
	//InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	/*Set Input Mode*/
	PLayerController->SetInputMode(InputModeData);
	PLayerController->bShowMouseCursor = false;
	PLayerController->SetPause(false);
	this->RemoveFromParent();

	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		MyGameInstance->FoodDeaths = MyGameInstance->StartFoodDeaths;
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level1st"))
	{
		UGameplayStatics::OpenLevel(GetWorld(), "Level1st");
	}
	
	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level2nd"))
	{
		UGameplayStatics::OpenLevel(GetWorld(), "Level2nd");
	}
	
	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level3rd"))
	{
		UGameplayStatics::OpenLevel(GetWorld(), "Level3rd");
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level4th"))
	{
		UGameplayStatics::OpenLevel(GetWorld(), "Level4th");
	}

}


void UGameOverMenu::QuitToMainClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "LevelMainMenu");
}

void UGameOverMenu::HideBtn()
{
	//BtnStartAgain->SetVisibility(ESlateVisibility::Hidden);
	BtnStartAgain->RemoveFromParent();

	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		MyGameInstance->FoodDeaths = MyGameInstance->StartFoodDeaths;
		MyGameInstance->Hits = 0;
	}
}