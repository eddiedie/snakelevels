// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverMenu.generated.h"

class UButton;

UCLASS()
class SNAKELEVELS_API UGameOverMenu : public UUserWidget
{
	GENERATED_BODY()

		virtual bool Initialize();

public:

	UPROPERTY(meta = (BindWidget))
		UButton* BtnStartAgain;
	UPROPERTY(meta = (BindWidget))
		UButton* BtnQuitToMain;

public:
	UFUNCTION()
		void StartAgainClicked();
	UFUNCTION()
		void QuitToMainClicked();

	UFUNCTION()
		void HideBtn();
	
};
