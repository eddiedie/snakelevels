// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenu.h"
#include "Components/Button.h"
#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"

bool UMainMenu::Initialize()
{
	Super::Initialize();

	BtnStartGame->OnClicked.AddDynamic(this, &UMainMenu::StartButtonClicked);
	BtnQuitGame->OnClicked.AddDynamic(this, &UMainMenu::QuitButtonClicked);

	return true;
}

void UMainMenu::StartButtonClicked()
{
	UE_LOG(LogTemp, Warning, TEXT("Our button is working!"));

	/*Get reference to PlayerController*/
	APlayerController* PLayerController = GetWorld()->GetGameInstance()->GetFirstLocalPlayerController();

	/*Set Up input parameters to the SetInputMode function*/
	FInputModeGameOnly InputModeData;
	//InputModeData.SetWidgetToFocus(MainMenu->TakeWidget());
	//InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	/*Set Input Mode*/
	PLayerController->SetInputMode(InputModeData);
	PLayerController->bShowMouseCursor = false;
	PLayerController->SetPause(false);
	this->RemoveFromParent();

	UGameplayStatics::OpenLevel(GetWorld(), "Level1st");
	//UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		//MyGameInstance->FoodDeaths = MyGameInstance->StartFoodDeaths;
		MyGameInstance->Hits = 0;
	}
	/*if (IsValid(MyGameInstance))
	{
		MyGameInstance->Lives = MyGameInstance->StartLives;
	}*/
}


void UMainMenu::QuitButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
}