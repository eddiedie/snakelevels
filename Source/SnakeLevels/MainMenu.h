// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

class UButton;

UCLASS()
class SNAKELEVELS_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()

		virtual bool Initialize();

public:

	UPROPERTY(meta = (BindWidget))
		UButton* BtnStartGame;
	UPROPERTY(meta = (BindWidget))
		UButton* BtnQuitGame;

public:
	UFUNCTION()
		void StartButtonClicked();
	UFUNCTION()
		void QuitButtonClicked();
	
};
