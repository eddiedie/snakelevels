// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstance.h"
#include "MainMenu.h"
#include "GameOverMenu.h"
#include "WinMenu.h"
#include "Counter.h"
#include "MyPawn.h"
#include "SnakeBase1st.h"
#include "Kismet/GameplayStatics.h"


/*--------------------------------MainMenu--------------------------------------*/

void UMyGameInstance::ShowMainMenu()
{
	MainMenu = CreateWidget<UMainMenu>(this, MainMenuClass);
	MainMenu->AddToViewport();
	//MainMenu->SetVisibility(ESlateVisibility::Visible);

	/*Get reference to PlayerController*/
	APlayerController* PLayerController = GetFirstLocalPlayerController();

	/*Set Up input parameters to the SetInputMode function*/
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(MainMenu->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	/*Set Input Mode*/
	PLayerController->SetInputMode(InputModeData);
	PLayerController->bShowMouseCursor = true;
	PLayerController->SetPause(true);
}

void UMyGameInstance::RemoveMainMenu()
{
	if (MainMenu)
	{
		MainMenu->RemoveFromParent();

		/*Set Input Mode*/
		APlayerController* PLayerController = GetFirstLocalPlayerController();
		PLayerController->bShowMouseCursor = false;
		PLayerController->SetPause(false);
	}
}


/*--------------------------------GameOverMenu--------------------------------------*/

void UMyGameInstance::ShowGameOverMenu()
{
	GameOverMenu = CreateWidget<UGameOverMenu>(this, GameOverMenuClass);
	GameOverMenu->AddToViewport();
	//MainMenu->SetVisibility(ESlateVisibility::Visible);

	/*Get reference to PlayerController*/
	APlayerController* PLayerController = GetFirstLocalPlayerController();

	/*Set Up input parameters to the SetInputMode function*/
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(GameOverMenu->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	/*Set Input Mode*/
	PLayerController->SetInputMode(InputModeData);
	PLayerController->bShowMouseCursor = true;
	PLayerController->SetPause(true);

	if (Lives <= 0)
	{
		GameOverMenu->HideBtn();
		Lives = StartLives;
		
	}

	
}

void UMyGameInstance::RemoveGameOverMenu()
{
	GameOverMenu->RemoveFromParent();

	/*Set Input Mode*/
	APlayerController* PLayerController = GetFirstLocalPlayerController();
	PLayerController->bShowMouseCursor = false;
	PLayerController->SetPause(false);
}


/*--------------------------------Win Menu--------------------------------------*/

void UMyGameInstance::ShowWinMenu()
{
	WinMenu = CreateWidget<UWinMenu>(this, WinMenuClass);
	WinMenu->AddToViewport();
	//MainMenu->SetVisibility(ESlateVisibility::Visible);

	/*Get reference to PlayerController*/
	APlayerController* PLayerController = GetFirstLocalPlayerController();

	/*Set Up input parameters to the SetInputMode function*/
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(WinMenu->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	/*Set Input Mode*/
	PLayerController->SetInputMode(InputModeData);
	PLayerController->bShowMouseCursor = true;
	PLayerController->SetPause(true);

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level4th"))
	{
		WinMenu->HideNextGameBtn();
	}
	
}

void UMyGameInstance::RemoveWinMenu()
{
	WinMenu->RemoveFromParent();

	/*Set Input Mode*/
	APlayerController* PLayerController = GetFirstLocalPlayerController();
	PLayerController->bShowMouseCursor = false;
	PLayerController->SetPause(false);
}


/*--------------------------------Counter--------------------------------------*/
void UMyGameInstance::ShowCounter()
{
	Counter = CreateWidget<UCounter>(this, CounterClass);
	Counter->AddToViewport();

}

void UMyGameInstance::UpdateHits()
{
	Counter->UpdateTextHit();
}

void UMyGameInstance::UpdateLives()
{
	Counter->UpdateTextLives();

	
	
}




void UMyGameInstance::UpdateFoodDeaths()
{
	Counter->UpdateTextFoodDeaths();
	
}

void UMyGameInstance::UpdateFoodNumber()
{
	Counter->UpdateTextFoodNumCounter();

	if (FoodNumber <= 0)
	{
		ShowWinMenu();
	}
}

