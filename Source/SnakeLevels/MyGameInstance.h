// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"


class UMainMenu;
class UGameOverMenu;
class UWinMenu;
class UCounter;

UCLASS()
class SNAKELEVELS_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY()						UMainMenu* MainMenu;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<UMainMenu>MainMenuClass;

	UPROPERTY()						UGameOverMenu* GameOverMenu;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<UGameOverMenu>GameOverMenuClass;

	UPROPERTY()						UWinMenu* WinMenu;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<UWinMenu>WinMenuClass;


	UPROPERTY()						UCounter* Counter;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<UCounter>CounterClass;
	UPROPERTY()						int32 Hits = 0;

	UPROPERTY()						int32 StartLives = 3;
	UPROPERTY()						int32 Lives = StartLives;

	UPROPERTY()						int32 StartFoodDeaths;			// Number of left food deaths on start game
	UPROPERTY()						int32 FoodDeaths = StartFoodDeaths; // Number of left food deaths to decrease by 1 life

	UPROPERTY()						int32 FoodNumber;


public:

	UFUNCTION()		void ShowMainMenu();
	UFUNCTION()		void RemoveMainMenu();

	UFUNCTION()		void ShowGameOverMenu();
	UFUNCTION()		void RemoveGameOverMenu();

	UFUNCTION()		void ShowWinMenu();
	UFUNCTION()		void RemoveWinMenu();

	UFUNCTION()		void ShowCounter();
	UFUNCTION()		void UpdateHits();
	UFUNCTION()		void UpdateLives();
	

	UFUNCTION()		void UpdateFoodDeaths();

	UFUNCTION()		void UpdateFoodNumber();
	
};
