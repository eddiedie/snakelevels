// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"
#include "SnakeBase1st.h"
#include "WinMenu.h"
#include "SnakeElement.h"

#include "Engine/Classes/Camera/CameraComponent.h"
#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"
//#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Engine/Classes/Components/InputComponent.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MyCamera"));
	RootComponent = MyCamera;

	
	
	//MyCamera->SetRelativeLocation(FVector(0, 0, 1000));
	MyCamera->SetRelativeRotation(FRotator(-90, 0, 0));


	//static ConstructorHelpers::FClassFinder<ASnakeBase1st>MySnakeBaseFinder(TEXT("Blueprint'/Game/BP_SnakeBase2nd.BP_SnakeBase2nd'"));
	//SnakeBase1stClass = MySnakeBaseFinder.Class;

	//static ConstructorHelpers::FObjectFinder<UClass>MySnake1stFinder(TEXT("Class'/Game/BP_SnakeBase1st.BP_SnakeBase1st_C'"));
	//SnakeBase1stClass = MySnake1stFinder.Object;

	//static ConstructorHelpers::FClassFinder<UClass>MySnake2ndFinder(TEXT("Class'/Game/BP_SnakeBase2nd.BP_SnakeBase2nd_C'"));
	//SnakeBase1stClass = MySnake2ndFinder.Object;
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	ShowHideMainMenu();
	CreateSnakeBase();
	AddCounterHits();

	CameraPose();
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	//MyCamera->AddWorldOffset(FVector(0, 0, 0), false, nullptr, ETeleportType::None);
}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &AMyPawn::PlayerInputVertical);
	PlayerInputComponent->BindAxis("Horizontal", this, &AMyPawn::PlayerInputHorizontal);
}



void AMyPawn::CameraPose()
{
	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level1st"))
	{
		if (SnakeBase1st)
		{
			MyCamera->SetWorldLocation(FVector(0, 0, CamPosLevel1st));
		}
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level2nd"))
	{
		if (SnakeBase2nd)
		{
			MyCamera->SetWorldLocation(FVector(0, 0, CamPosLevel2nd));
		}
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level3rd"))
	{
		if (SnakeBase3rd)
		{
			MyCamera->SetWorldLocation(FVector(0, 0, CamPosLevel3rd));
			MyCamera->SetWorldLocation(FVector(SnakeBase3rd->SnakeElementArray[0]->GetActorLocation().X, SnakeBase3rd->SnakeElementArray[0]->GetActorLocation().Y, CamPosLevel3rd));
		}
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level4th"))
	{
		if (SnakeBase4th)
		{
			MyCamera->SetWorldLocation(FVector(0, 0, CamPosLevel4th));
		}
	}
}





/*-------------------------------------Main Menu-------------------------------------------------*/

void AMyPawn::ShowHideMainMenu()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("LevelMainMenu"))
		{
			MyGameInstance->ShowMainMenu();
			GEngine->AddOnScreenDebugMessage(2, 10, FColor::Green, "Main Menu is started.");
		}
		else
		{
			MyGameInstance->RemoveMainMenu();
		}
	}
}



/*------------------------------------Game Over Menu-------------------------------------------*/

//void AMyPawn::ShowHideGameOverMenu()
//{
//	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
//	if (IsValid(MyGameInstance))
//	{
//		if (!UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("LevelMainMenu"))
//		{
//			MyGameInstance->ShowGameOverMenu();
//			GEngine->AddOnScreenDebugMessage(2, 10, FColor::Green, "Game Over Menu is started.");
//		}
//
//		if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("LevelMainMenu"))
//		{
//			MyGameInstance->RemoveGameOverMenu();
//		}
//	}
//}



/*---------------------------------------Win Menu------------------------------------------*/

//void AMyPawn::ShowWinMenu()
//{
//	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
//	if (IsValid(MyGameInstance))
//	{
//		if (!UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("LevelMainMenu"))
//		{
//			MyGameInstance->ShowWinMenu();
//			GEngine->AddOnScreenDebugMessage(2, 10, FColor::Green, "Win Menu is started.");
//		}
//
//		else /*if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("LevelMainMenu"))*/
//		{
//			MyGameInstance->RemoveWinMenu();
//		}
//
//		TArray<UUserWidget*> UMGWidgets;
//		UWidgetBlueprintLibrary::GetAllWidgetsOfClass(GetWorld(), UMGWidgets, UWinMenu::StaticClass(), true);
//		for (int32 i = 0; i < UMGWidgets.Num(); i++)
//		{
//			auto wasd = UMGWidgets[i]->GetName();
//			GEngine->AddOnScreenDebugMessage(2, 10, FColor::Black, wasd);
//
//			UWinMenu* WinMenu = Cast<UWinMenu>(UMGWidgets[i]);
//			if (IsValid(WinMenu))
//			{
//				GEngine->AddOnScreenDebugMessage(1, 10, FColor::Red, "Win Menu is Casted.");
//				break;
//			}
//		}
//
//	}
//}




void AMyPawn::AddCounterHits()
{
	if (!UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("LevelMainMenu"))
	{
		UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		if (IsValid(MyGameInstance))
		{
			MyGameInstance->ShowCounter();
		}
	}
}


/*-------------------Snake Base-------------------*/

void AMyPawn::CreateSnakeBase()
{
	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level1st"))
	{
		SnakeBase1st = GetWorld()->SpawnActor<ASnakeBase1st>(SnakeBase1stClass, FTransform());
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level2nd"))
	{
		SnakeBase2nd = GetWorld()->SpawnActor<ASnakeBase1st>(SnakeBase2ndClass, FTransform());
	}
	
	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level3rd"))
	{
		SnakeBase3rd = GetWorld()->SpawnActor<ASnakeBase1st>(SnakeBase3rdClass, FTransform());
	}

	if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level4th"))
	{
		SnakeBase4th = GetWorld()->SpawnActor<ASnakeBase1st>(SnakeBase4thClass, FTransform());
	}
	
}

void AMyPawn::PlayerInputVertical(float value)
{
	if (IsValid(SnakeBase1st))
	{
		if (value > 0 && SnakeBase1st->LastMoveDirection != MoveDirection::DOWN)
		{
			SnakeBase1st->LastMoveDirection = MoveDirection::UP;
		}
		else if (value < 0 && SnakeBase1st->LastMoveDirection != MoveDirection::UP)
		{
			SnakeBase1st->LastMoveDirection = MoveDirection::DOWN;
		}
	}

	if (IsValid(SnakeBase2nd))
	{
		if (value > 0 && SnakeBase2nd->LastMoveDirection != MoveDirection::DOWN)
		{
			SnakeBase2nd->LastMoveDirection = MoveDirection::UP;
		}
		else if (value < 0 && SnakeBase2nd->LastMoveDirection != MoveDirection::UP)
		{
			SnakeBase2nd->LastMoveDirection = MoveDirection::DOWN;
		}
	}

	if (IsValid(SnakeBase3rd))
	{
		if (value > 0 && SnakeBase3rd->LastMoveDirection != MoveDirection::DOWN)
		{
			SnakeBase3rd->LastMoveDirection = MoveDirection::UP;
		}
		else if (value < 0 && SnakeBase3rd->LastMoveDirection != MoveDirection::UP)
		{
			SnakeBase3rd->LastMoveDirection = MoveDirection::DOWN;
		}
	}

	if (IsValid(SnakeBase4th))
	{
		if (value > 0 && SnakeBase4th->LastMoveDirection != MoveDirection::DOWN)
		{
			SnakeBase4th->LastMoveDirection = MoveDirection::UP;
		}
		else if (value < 0 && SnakeBase4th->LastMoveDirection != MoveDirection::UP)
		{
			SnakeBase4th->LastMoveDirection = MoveDirection::DOWN;
		}
	}
}

void AMyPawn::PlayerInputHorizontal(float value)
{
	if (IsValid(SnakeBase1st))
	{
		if (value > 0 && SnakeBase1st->LastMoveDirection != MoveDirection::LEFT)
		{
			SnakeBase1st->LastMoveDirection = MoveDirection::RIGHT;
		}
		else if (value < 0 && SnakeBase1st->LastMoveDirection != MoveDirection::RIGHT)
		{
			SnakeBase1st->LastMoveDirection = MoveDirection::LEFT;
		}
	}

	if (IsValid(SnakeBase2nd))
	{
		if (value > 0 && SnakeBase2nd->LastMoveDirection != MoveDirection::LEFT)
		{
			SnakeBase2nd->LastMoveDirection = MoveDirection::RIGHT;
		}
		else if (value < 0 && SnakeBase2nd->LastMoveDirection != MoveDirection::RIGHT)
		{
			SnakeBase2nd->LastMoveDirection = MoveDirection::LEFT;
		}
	}

	if (IsValid(SnakeBase3rd))
	{
		if (value > 0 && SnakeBase3rd->LastMoveDirection != MoveDirection::LEFT)
		{
			SnakeBase3rd->LastMoveDirection = MoveDirection::RIGHT;
		}
		else if (value < 0 && SnakeBase3rd->LastMoveDirection != MoveDirection::RIGHT)
		{
			SnakeBase3rd->LastMoveDirection = MoveDirection::LEFT;
		}
	}

	if (IsValid(SnakeBase4th))
	{
		if (value > 0 && SnakeBase4th->LastMoveDirection != MoveDirection::LEFT)
		{
			SnakeBase4th->LastMoveDirection = MoveDirection::RIGHT;
		}
		else if (value < 0 && SnakeBase4th->LastMoveDirection != MoveDirection::RIGHT)
		{
			SnakeBase4th->LastMoveDirection = MoveDirection::LEFT;
		}
	}
}

