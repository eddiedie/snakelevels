// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UObject/ConstructorHelpers.h"
#include "CoreMinimal.h"

#include "MyGameInstance.h"
#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"


class UCameraComponent;
class ASnakeBase1st;

UCLASS()
class SNAKELEVELS_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawn();

	
	
	UPROPERTY()		UCameraComponent* MyCamera;

	/*-------------------- Variables to set pose of camera of different levels in BP_MyPawn ----------------------*/
	UPROPERTY(EditDefaultsOnly) int CamPosLevel1st;
	UPROPERTY(EditDefaultsOnly) int CamPosLevel2nd;
	UPROPERTY(EditDefaultsOnly) int CamPosLevel3rd;
	UPROPERTY(EditDefaultsOnly) int CamPosLevel4th;

	
		
	UPROPERTY(BlueprintReadWrite)	ASnakeBase1st* SnakeBase1st;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<ASnakeBase1st>SnakeBase1stClass;

	UPROPERTY(BlueprintReadWrite)	ASnakeBase1st* SnakeBase2nd;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<ASnakeBase1st>SnakeBase2ndClass;

	UPROPERTY(BlueprintReadWrite)	ASnakeBase1st* SnakeBase3rd;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<ASnakeBase1st>SnakeBase3rdClass;

	UPROPERTY(BlueprintReadWrite)	ASnakeBase1st* SnakeBase4th;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<ASnakeBase1st>SnakeBase4thClass;
	
	
	//static ConstructorHelpers::FClassFinder<ASnakeBase1st>MySnakeBaseFinder(TEXT("Blueprint'/Game/BP_SnakeBase2nd.BP_SnakeBase2nd'"));
	//SnakeBase1stClass = MySnakeBaseFinder.ThisClass;
	
	

	//UPROPERTY(BlueprintReadWrite)	ASnakeBase1st* SnakeBase2nd;
	//UPROPERTY(EditDefaultsOnly)		TSubclassOf<ASnakeBase1st>SnakeBase2ndClass;

	//UPROPERTY()
		//TSoftClassPtr<ASnakeBase1st> MySnakeBase = TSoftClassPtr<ASnakeBase1st>(FSoftObjectPath(TEXT("Blueprint'/Game/BP_SnakeBase2nd.BP_SnakeBase2nd'")));
	//UPROPERTY()
	//static ConstructorHelpers::FClassFinder<ASnakeBase1st>MySnakeBaseFinder(TEXT("Blueprint'/Game/BP_SnakeBase2nd.BP_SnakeBase2nd'"));
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	/*-------------------Main Menu-------------------*/
	void ShowHideMainMenu();

	/*-------------------Game Over Menu-------------------*/
	//void ShowHideGameOverMenu();

	/*-------------------Win Menu-------------------*/
	//void ShowWinMenu();

	/*--------------------Counter--------------------------*/
	void AddCounterHits();


	/*-------------------Snake Base-------------------*/

	UFUNCTION(BlueprintCallable)
	void CreateSnakeBase();
	UFUNCTION()
		void PlayerInputVertical(float value);
	UFUNCTION()
		void PlayerInputHorizontal(float value);
	void CameraPose();
	
};
