// Fill out your copyright notice in the Description page of Project Settings.


#include "Portal.h"

#include "MyGameInstance.h"
#include "SnakeInterface.h"
#include "SnakeBase1st.h"
#include "SnakeElement.h"

// Sets default values
APortal::APortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}







void APortal::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase1st* Snake = Cast<ASnakeBase1st>(Interactor);
		if (IsValid(Snake))
		{
			PortalOwner->PortalArray.Remove(this);
			
			int32 PosNo = rand() % PortalOwner->PortalArray.Num();
			
			auto Pos = PortalOwner->PortalArray[PosNo]->GetActorLocation();
			auto SnakeHead = Snake->SnakeElementArray[0];
			
			//Snake->LastMoveDirection = (MoveDirection)FMath::FRandRange(0, 3);
			//uint8 n = FMath::RandRange(static_cast<int>(MoveDirection::UP), static_cast<int>(MoveDirection::RIGHT) - 1);
			//Snake->LastMoveDirection = static_cast<MoveDirection>(n);

			if (Snake->LastMoveDirection == MoveDirection::UP)
			{
				Pos.X += Snake->SnakeSize;
			}
			if (Snake->LastMoveDirection == MoveDirection::DOWN)
			{
				Pos.X -= Snake->SnakeSize;
			}
			if (Snake->LastMoveDirection == MoveDirection::LEFT)
			{
				Pos.Y += Snake->SnakeSize;
			}
			if (Snake->LastMoveDirection == MoveDirection::RIGHT)
			{
				Pos.Y -= Snake->SnakeSize;
			}

			if (Snake->SnakeElementArray.Num() > 1)
			{
				SnakeHead->SetActorLocation(Pos);

				auto LastElement = Snake->SnakeElementArray.Last();
				int32 Last = Snake->SnakeElementArray.Num() - 1;
				
				LastElement->Destroy();
				Snake->SnakeElementArray.RemoveAt(Last);

				//PortalOwner->Portal->SetActorLabel("PortalFrozen");
				PortalOwner->Portal->Tags.Add(FName("PortalFrozen"));

				UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
				if (IsValid(MyGameInstance))
				{

					GEngine->AddOnScreenDebugMessage(3, 5, FColor::Red, "MyGameInstance Casted!");

					MyGameInstance->FoodDeaths++;
					MyGameInstance->UpdateFoodDeaths();

					MyGameInstance->FoodNumber++;
					MyGameInstance->UpdateFoodNumber();

				}

				
				GEngine->AddOnScreenDebugMessage(2, 5, FColor::Red, "PORTAL FROZEN");
				int32 SnakeTailLength = Snake->SnakeElementArray.Num();
				PortalOwner->PortalArray.Add(this);
				Snake->SetRenamePortal(SnakeTailLength);

				PortalOwner->CreateFoodOnPortal();
				
			}
			else
			{
				PortalOwner->PortalArray.Add(this);
			}
		}
	}
}
