// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase1st.h"
#include "SnakeElement.h"
#include "Food.h"
#include "FoodDeath.h"
#include "MyGameInstance.h"
#include "WallDeath.h"
#include "WallPortal.h"
#include "Walker.h"
#include "Portal.h"
#include "Math/UnrealMathUtility.h"


// Sets default values
ASnakeBase1st::ASnakeBase1st()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void ASnakeBase1st::BeginPlay()
{
	Super::BeginPlay();
	
	SetFoodDeaths(FoodDeathsLeftNum);
	CreateSnakeArray(SnakeNum);
	//SetSnakeLives(SnakeLives);

	CreateFoodArray(FoodXRange, FoodYRange, SnakeSize);
	DestroyFoodIntersections();
	SetupNumberFood(FoodNum);
	SetFoodCount();
	SetFoodToRed(FoodRed, FoodNum);

	
	
	CreateWall(FoodXRange, FoodYRange, SnakeSize);

	
	
	CreateWalker(WalkerNum);
	CreatePortal(PortalNum);
	CreateFoodDeathWall(FoodDeathWallNum);

	if (FoodDelaySetUp == true)
	{
		FoodMovePlay();
	}
	
	if (WalkerDelaySetUp == true)
	{
		WalkerMovePlay();
	}

	if (PortalDelaySetUp == true)
	{
		PortalMovePlay();
	}

	if (FoodDeathDelaySetUp == true)
	{
		FoodDeathMovePlay();
	}
	
	
}

// Called every frame
void ASnakeBase1st::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(SnakeSpeed);

	SnakeMove();

	if (WalkerDelaySetUp == false)
	{
		WalkerMove();
	}

	if (FoodDelaySetUp == false)
	{
		FoodMove();
	}

	if (PortalDelaySetUp == false)
	{
		PortalMove();
	}

	if (FoodDeathDelaySetUp == false)
	{
		FoodDeathMove();
	}
}




/*----------------------------------Snake----------------------------------*/


void ASnakeBase1st::SetSnakeLives(int32 SetLives)
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		GEngine->AddOnScreenDebugMessage(0, 2, FColor::Black, "Start Lives Casted!");
		
		MyGameInstance->StartLives = SetLives;
		GEngine->AddOnScreenDebugMessage(1, 3, FColor::Red, FString::FromInt(MyGameInstance->StartLives));
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(0, 2, FColor::Black, "Start Lives FAILED!");
	}
}



void ASnakeBase1st::CreateSnakeArray(int SnakeNumber)
{
	FRotator MoveRotation(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case MoveDirection::UP:

		MoveRotation.Yaw = 0;
		break;
	case MoveDirection::DOWN:

		MoveRotation.Yaw = 180;
		break;
	case MoveDirection::LEFT:

		MoveRotation.Yaw = 90;
		break;
	case MoveDirection::RIGHT:

		MoveRotation.Yaw = -90;
		break;
	}

	FVector NewLocation(SnakeSize * SnakeElementArray.Num(), 0, 0);
	FTransform NewTransform(MoveRotation, NewLocation);
	
	for (int i = 0; i < SnakeNumber; i++)
	{
		SnakeElement = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTransform);
		
		SnakeElement->SetActorScale3D(FVector(SnakeSize* 0.009));
		SnakeElement->SnakeOwner = this;

		int32 ElementIndex = SnakeElementArray.Add(SnakeElement);

		if (ElementIndex == 0)
		{
			SnakeElement->SetHead();
		}
	}
}

	
	
void ASnakeBase1st::SnakeMove()
{
	FVector		Direction(ForceInitToZero);
	FRotator	MoveRotation(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case MoveDirection::UP:
		Direction.X += SnakeSize;
		MoveRotation.Yaw = 0;
		break;
	case MoveDirection::DOWN:
		Direction.X -= SnakeSize;
		MoveRotation.Yaw = 180;
		break;
	case MoveDirection::LEFT:
		Direction.Y += SnakeSize;
		MoveRotation.Yaw = 90;
		break;
	case MoveDirection::RIGHT:
		Direction.Y -= SnakeSize;
		MoveRotation.Yaw = -90;
		break;
	}

	SnakeElementArray[0]->ToggleCollision();

	for (int i = SnakeElementArray.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElementArray[i];
		auto PrevElement = SnakeElementArray[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		FRotator PrevRotation = PrevElement->GetActorRotation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorRotation(PrevRotation);
	}

	SnakeElementArray[0]->AddActorWorldOffset(Direction);
	SnakeElementArray[0]->SetActorRotation(MoveRotation);

	SnakeElementArray[0]->ToggleCollision();
}


void ASnakeBase1st::GrowSnakeOnFood()
{
	auto LastLocation = SnakeElementArray.Last()->GetActorLocation();
	auto LastRotation = SnakeElementArray.Last()->GetActorRotation();


	int LastRot = FMath::RoundToInt(LastRotation.Yaw);


	if (LastRot == 0)
	{
		LastLocation.X -= SnakeSize;
	}
	else if (LastRot == -180)
	{
		LastLocation.X += SnakeSize;
	}
	else if (LastRot == 90)
	{
		LastLocation.Y -= SnakeSize;
	}
	else if (LastRot == -90)
	{
		LastLocation.Y += SnakeSize;
	}

	//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Black, FString::Printf(TEXT("LastRotation: %s"), *LastRotation.ToString()));
	//GEngine->AddOnScreenDebugMessage(2, 5, FColor::Blue, FString::FromInt(LastRot));

	SnakeElement = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, FTransform(LastRotation, LastLocation));
	SnakeElement->SetActorScale3D(FVector(SnakeSize * 0.009));
	SnakeElementArray.Add(SnakeElement);
	SnakeElement->SnakeOwner = this;

}


/*------------------------ Create Food ------------------------*/

void ASnakeBase1st::CreateFoodArray(int XRange, int YRange, int FSize)
{
	XRange -= XRange % FSize;
	YRange -= YRange % FSize;

	int XrangeLocation = -XRange;
	int YrangeLocation = -YRange;

	int FoodNumX = XRange * 2 / FSize + 1;
	int FoodNumY = YRange * 2 / FSize;

	for (int x = 0; x < FoodNumX; x++)
	{
		int SpawnX = XrangeLocation;
		int SpawnY = YrangeLocation;

		FVector SpawnLocation(SpawnX, SpawnY, 0);
		FTransform SpawnTransform(SpawnLocation);
		Food = GetWorld()->SpawnActor<AFood>(FoodClass, SpawnTransform);
		Food->SetActorScale3D(FVector(SnakeSize * 0.009));

		//Food->SetActorLabel("FoodMoved");
		Food->Tags.Add(FName("FoodMoved"));

		Food->FoodOwner = this;
		FoodArray.Add(Food);

		for (int y = 0; y < FoodNumY; y++)
		{
			SpawnX = XrangeLocation;
			SpawnY = YrangeLocation + FSize;

			SpawnLocation = FVector(SpawnX, SpawnY, 0);
			SpawnTransform = FTransform(SpawnLocation);
			Food = GetWorld()->SpawnActor<AFood>(FoodClass, SpawnTransform);
			Food->SetActorScale3D(FVector(SnakeSize * 0.009));
			
			//Food->SetActorLabel("FoodMoved");
			Food->Tags.Add(FName("FoodMoved"));
			
			Food->FoodOwner = this;
			
			FoodArray.Add(Food);

			YrangeLocation += FSize;
		}

		XrangeLocation += FSize;
		YrangeLocation -= FSize * FoodNumY;
	}

}


void ASnakeBase1st::DestroyFoodIntersections()
{
	for (int i = 0; i < SnakeElementArray.Num(); i++)
	{
		auto SnakePos = SnakeElementArray[i]->GetActorLocation();

		for (int x = 0; x < FoodArray.Num(); x++)
		{
			auto FoodPos = FoodArray[x]->GetActorLocation();

			if (FoodPos == SnakePos)
			{
				FoodArray[x]->Destroy();
				FoodArray.RemoveAt(x);
			}
		}
	}


	
		
}


void ASnakeBase1st::SetupNumberFood(int NumberOfFood)
{
	int FoodNumberDecrement = FoodArray.Num() - NumberOfFood;
	int LastIndex = FoodArray.Num() - 1;

	for (int i = 0; i <= LastIndex; i++)
	{
		int Index = FMath::FRandRange(i, LastIndex);
		if (i != Index)
		{
			FoodArray.Swap(i, Index);
		}
	}
	for (int i = 0; i < FoodNumberDecrement; i++)
	{
		FoodArray[0]->Destroy();
		FoodArray.RemoveAt(0, 1);
	}
}


void ASnakeBase1st::SetFoodCount()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		int32 StartFoodNum = FoodArray.Num();
		MyGameInstance->FoodNumber = StartFoodNum;
			
	}
}




void ASnakeBase1st::SetFoodToRed(int FRed, int FCount)
{
	if (FRed >= FCount)
	{
		FRed = FCount;
	}
	
	for (int i = 0; i < FRed; i++)
	{
		FoodArray[i]->SetFoodRed();

		//FoodArray[i]->SetActorLabel("SetSpeed");
		FoodArray[i]->Tags.Add(FName("SetSpeed"));
		
	}
	
}




void ASnakeBase1st::FoodMove()
{
	for (int i = 0; i < FoodArray.Num(); ++i)
	{
		const uint8 RandomDirection = FMath::RandRange(0, 3);
		FoodMoving = static_cast<EFoodMoving>(RandomDirection);
		
		FVector		FDirection(ForceInitToZero);
		FRotator	FMoveRotation(ForceInitToZero);
		
		switch (FoodMoving)
		{
		case EFoodMoving::FoodUP:
			FDirection.X += SnakeSize;
			FMoveRotation.Yaw = 0;
			
			break;
			
		case EFoodMoving::FoodDOWN:
			FDirection.X -= SnakeSize;
			FMoveRotation.Yaw = 180;
			
			break;
			
		case EFoodMoving::FoodLEFT:
			FDirection.Y += SnakeSize;
			FMoveRotation.Yaw = 90;
			
			break;
			
		case EFoodMoving::FoodRIGHT:
			FDirection.Y -= SnakeSize;
			FMoveRotation.Yaw = -90;
			
			break;
		}

		
		if (FoodArray[i])
		{
			bool const RandMove = rand() % 2 == 0;

			if (RandMove == 1)
			{
				FoodArray[i]->AddActorWorldOffset(FDirection);
				FoodArray[i]->SetActorRotation(FMoveRotation);
				const FVector FoodPos = FoodArray[i]->GetActorLocation();

			
				if (FoodPos.X > FoodXRange || FoodPos.X < -FoodXRange)
				{
					FoodArray[i]->AddActorWorldOffset(-FDirection);
				}
				else if (FoodPos.Y > FoodYRange || FoodPos.Y < -FoodYRange)
				{
					FoodArray[i]->AddActorWorldOffset(-FDirection);
				}
			}
		
		}
		
	}
}


void ASnakeBase1st::FoodMovePlay()
{
	FTimerHandle FoodTimer;
	GetWorldTimerManager().SetTimer(FoodTimer, this, &ASnakeBase1st::FoodMove, FoodMoveDelay, true, 1);
}




void ASnakeBase1st::RemoveDestroyed(AFood* ToRemove)
{
	
	FoodArray.Remove(ToRemove);
}



void ASnakeBase1st::CreateFoodOnPortal()
{
	int subXPos = FMath::FRandRange(-FoodXRange, FoodXRange);
	subXPos -= subXPos % SnakeSize;
	int subYPos = FMath::FRandRange(-FoodYRange, FoodYRange);
	subYPos -= subYPos % SnakeSize;

	FVector Location(subXPos, subYPos, 0);

	FTransform NewTransform(Location);
	Food = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	Food->SetActorScale3D(FVector(SnakeSize * 0.009));

	//Food->SetActorLabel("FoodMoved");
	Food->Tags.Add(FName("FoodMoved"));
	
	Food->FoodOwner = this;

	FoodArray.Add(Food);
}




void ASnakeBase1st::SetFoodDeaths(int32 SetFDeaths)
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		MyGameInstance->FoodDeaths = SetFDeaths;
		
	}
}






/*------------------------ Create FoodDeath ------------------------*/


void ASnakeBase1st::CreateFoodDeathWall(int FDNum)
{
	for (int x = 0; x < FDNum; x++)
	{
		int subXPos = FMath::FRandRange(-FoodXRange, FoodXRange);
		subXPos -= subXPos % SnakeSize;
		int subYPos = FMath::FRandRange(-FoodYRange, FoodYRange);
		subYPos -= subYPos % SnakeSize;

		FVector Location(subXPos, subYPos, 0);

		FTransform NewTransform(Location);
		FoodDeath = GetWorld()->SpawnActor<AFoodDeath>(FoodDeathClass, NewTransform);
		FoodDeath->SetActorScale3D(FVector(SnakeSize * 0.009));
		FoodDeath->FoodDeathOwner = this;

		FoodDeathArray.Add(FoodDeath);
	}
}

void ASnakeBase1st::FoodDeathMove()
{
	if (FoodDeath)
	{
		for (int i = 0; i < FoodDeathArray.Num(); i++)
		{
			//int wasd = FMath::FRandRange()
			auto FoodDeathDirection = MoveDirection(rand() % 4);

			FVector Direction(ForceInitToZero);

			switch (FoodDeathDirection)
			{
			case MoveDirection::UP:
				Direction.X += SnakeSize;
				break;
			case MoveDirection::DOWN:
				Direction.X -= SnakeSize;
				break;
			case MoveDirection::LEFT:
				Direction.Y += SnakeSize;
				break;
			case MoveDirection::RIGHT:
				Direction.Y -= SnakeSize;
				break;
			}

			bool const RandMove = rand() % 2 == 0;

			if (RandMove == 0)
			{
				FoodDeathArray[i]->AddActorWorldOffset(Direction);
				FVector FoodDeathPos = FoodDeathArray[i]->GetActorLocation();

				if (FoodDeathPos.X > FoodXRange || FoodDeathPos.X < -FoodXRange)
				{
					FoodDeathArray[i]->AddActorWorldOffset(-Direction);
				}
				else if (FoodDeathPos.Y > FoodYRange || FoodDeathPos.Y < -FoodYRange)
				{
					FoodDeathArray[i]->AddActorWorldOffset(-Direction);
				}
			}
		}
	}
}



void ASnakeBase1st::FoodDeathMovePlay()
{
	FTimerHandle FoodDeathTimer;
	GetWorldTimerManager().SetTimer(FoodDeathTimer, this, &ASnakeBase1st::FoodDeathMove, FoodDeathMoveDelay, true, 1);
}





/*------------------------ Create Wall ------------------------*/


void ASnakeBase1st::CreateWall(int XRange, int YRange, int WSize)
{
	XRange -= XRange % WSize - WSize;
	YRange -= YRange % WSize - WSize;

	int XrangeLocation = -XRange;
	int YrangeLocation = -YRange;

	int WallNumX = XRange * 2 / WSize + 1;
	int WallNumY = YRange * 2 / WSize;

	for (int x = 0; x < WallNumX; x++)
	{
		int SpawnX = XrangeLocation;
		int SpawnY = YrangeLocation;

		FVector SpawnLocation(SpawnX, SpawnY, 0);
		FTransform SpawnTransform(SpawnLocation);
		WallDeath = GetWorld()->SpawnActor<AWallDeath>(WallDeathClass, SpawnTransform);
		WallDeath->SetActorScale3D(FVector(SnakeSize * 0.0095));
		//WallPortal->SetActorLabel("WallDeath");
		WallDeath->WallDeathOwner = this;
		
		WallDeathArray.Add(WallDeath);

		for (int y = 0; y < WallNumY; y++)
		{
			if (x == 0 || x == WallNumX - 1)
			{
				SpawnX = XrangeLocation;
				SpawnY = YrangeLocation + WSize;
				SpawnLocation = FVector(SpawnX, SpawnY, 0);
				SpawnTransform = FTransform(SpawnLocation);
				WallDeath = GetWorld()->SpawnActor<AWallDeath>(WallDeathClass, SpawnTransform);
				WallDeath->SetActorScale3D(FVector(SnakeSize * 0.0095));
				//WallPortal->SetActorLabel("WallDeath");
				WallDeath->WallDeathOwner = this;
				
				WallDeathArray.Add(WallDeath);
			}

			else
			{
				if (y == WallNumY - 1)
				{
					SpawnX = XrangeLocation;
					SpawnY = YrangeLocation + WSize;
					SpawnLocation = FVector(SpawnX, SpawnY, 0);
					SpawnTransform = FTransform(SpawnLocation);
					WallDeath = GetWorld()->SpawnActor<AWallDeath>(WallDeathClass, SpawnTransform);
					WallDeath->SetActorScale3D(FVector(SnakeSize * 0.0095));
					//WallPortal->SetActorLabel("WallDeath");
					WallDeath->WallDeathOwner = this;
					
					WallDeathArray.Add(WallDeath);
				}
			}

			YrangeLocation += WSize;
		}

		XrangeLocation += WSize;
		YrangeLocation -= WSize * WallNumY;
	}

	for (int i = 0; i < WallDeathArray.Num(); i++)
	{
		auto WallElement = WallDeathArray[i];
		FVector WallPose = WallElement->GetActorLocation();
		if (WallPose.X == 0 || WallPose.Y == 0)
		{
			WallPortal = GetWorld()->SpawnActor<AWallPortal>(WallPortalClass, FTransform(WallPose));
			WallPortal->SetActorScale3D(FVector(SnakeSize * 0.009));
			WallPortal->WallPortalOwner = this;
			
			WallPortalArray.Add(WallPortal);
			WallElement->Destroy();

			FVector WPLocation = WallPortal->GetActorLocation();

			if (WPLocation.Y < 0)
			{
				//WallPortal->SetActorLabel("WPLeft");
				WallPortal->Tags.Add(FName("WPLeft"));
			}
			if (WPLocation.Y > 0)
			{
				//WallPortal->SetActorLabel("WPRight");
				WallPortal->Tags.Add(FName("WPRight"));
			}
			if (WPLocation.X < 0)
			{
				//WallPortal->SetActorLabel("WPBot");
				WallPortal->Tags.Add(FName("WPBot"));
			}
			if (WPLocation.X > 0)
			{
				//WallPortal->SetActorLabel("WPTop");
				WallPortal->Tags.Add(FName("WPTop"));
			}
		}
	}
}


/*------------------------- Create Walker ---------------------------*/

void ASnakeBase1st::CreateWalker(int Num)
{
	for (int x = 0; x < Num; x++)
	{
		int subXPos = FMath::FRandRange(-FoodXRange, FoodXRange);
		subXPos -= subXPos % SnakeSize;
		int subYPos = FMath::FRandRange(-FoodYRange, FoodYRange);
		subYPos -= subYPos % SnakeSize;

		if (subYPos == 0)
		{
			subYPos += SnakeSize;
		}
		
		FVector Location(subXPos, subYPos, 0);

		FTransform NewTransform(Location);
		Walker = GetWorld()->SpawnActor<AWalker>(WalkerClass, NewTransform);
		Walker->SetActorScale3D(FVector(SnakeSize * 0.009));
		Walker->WalkerOwner = this;

		WalkerArray.Add(Walker);
	}
}


void ASnakeBase1st::WalkerMove()
{
	if (Walker)
	{
		for (int i = 0; i < WalkerArray.Num(); i++) 
		{
			//int wasd = FMath::FRandRange()
			auto WalkerDirection = MoveDirection(rand() % 4);

			FVector Direction(ForceInitToZero);

			switch (WalkerDirection)
			{
			case MoveDirection::UP:
				Direction.X += SnakeSize;
				break;
			case MoveDirection::DOWN:
				Direction.X -= SnakeSize;
				break;
			case MoveDirection::LEFT:
				Direction.Y += SnakeSize;
				break;
			case MoveDirection::RIGHT:
				Direction.Y -= SnakeSize;
				break;
			}

			bool const RandMove = rand() % 2 == 0;

			if (RandMove == 0)
			{
				WalkerArray[i]->AddActorWorldOffset(Direction);
				FVector WalkerPos = WalkerArray[i]->GetActorLocation();

				if (WalkerPos.X > FoodXRange || WalkerPos.X < -FoodXRange)
				{
					WalkerArray[i]->AddActorWorldOffset(-Direction);
				}
				else if (WalkerPos.Y > FoodYRange || WalkerPos.Y < -FoodYRange)
				{
					WalkerArray[i]->AddActorWorldOffset(-Direction);
				}
			}
		}
	}
}


void ASnakeBase1st::WalkerMovePlay()
{
	FTimerHandle WalkerTimer;
	GetWorldTimerManager().SetTimer(WalkerTimer, this, &ASnakeBase1st::WalkerMove, WalkerMoveDelay, true, 1);
	
}



/*------------------------- Create Portal ---------------------------*/

void ASnakeBase1st::CreatePortal(int Num)
{
	for (int i = 0; i < Num; i++)
	{
		int subXPos = FMath::FRandRange(-FoodXRange, FoodXRange);
		subXPos -= subXPos % SnakeSize;
		int subYPos = FMath::FRandRange(-FoodYRange, FoodYRange);
		subYPos -= subYPos % SnakeSize;

		if (subYPos == 0)
		{
			subYPos += SnakeSize;
		}

		FVector Location(subXPos, subYPos, 0);

		FTransform NewTransform(Location);
		Portal = GetWorld()->SpawnActor<APortal>(PortalClass, NewTransform);
		Portal->SetActorScale3D(FVector(SnakeSize * 0.009));

		//Portal->SetActorLabel("PortalMoving");
		Portal->Tags.Add(FName("PortalMoving"));

		Portal->PortalOwner = this;

		PortalArray.Add(Portal);
	}
}

void ASnakeBase1st::PortalMove()
{
	if (Portal)
	{
		if (Portal->ActorHasTag(FName("PortalMoving")) /*Portal->GetActorLabel() == "PortalMoving"*/)
		{
			for (int i = 0; i < PortalArray.Num(); i++)
			{
				auto PortalDirection = MoveDirection(rand() % 4);

				FVector Direction(ForceInitToZero);

				switch (PortalDirection)
				{
				case MoveDirection::UP:
					Direction.X += SnakeSize;
					break;
				case MoveDirection::DOWN:
					Direction.X -= SnakeSize;
					break;
				case MoveDirection::LEFT:
					Direction.Y += SnakeSize;
					break;
				case MoveDirection::RIGHT:
					Direction.Y -= SnakeSize;
					break;
				}

				if (PortalArray[i])
				{
					bool const RandMove = rand() % 2 == 0;

					if (RandMove == 0)
					{
						PortalArray[i]->AddActorWorldOffset(Direction);
						FVector PortalPos = PortalArray[i]->GetActorLocation();

						if (PortalPos.X > FoodXRange || PortalPos.X < -FoodXRange)
						{
							PortalArray[i]->AddActorWorldOffset(-Direction);
						}
						else if (PortalPos.Y > FoodYRange || PortalPos.Y < -FoodYRange)
						{
							PortalArray[i]->AddActorWorldOffset(-Direction);
						}
					}
				}
			}
		}
	}
}


void ASnakeBase1st::PortalMovePlay()
{
	FTimerHandle PortalTimer;
	GetWorldTimerManager().SetTimer(PortalTimer, this, &ASnakeBase1st::PortalMove, PortalMoveDelay, true, 1);
}


void ASnakeBase1st::SetRenamePortal(int32 SnakeTailLength)
{
	
	FTimerHandle RenameTimer;
	
	GetWorldTimerManager().SetTimer(RenameTimer, this, &ASnakeBase1st::RenamePortal, 1, false, SnakeTailLength/2);
}


void ASnakeBase1st::RenamePortal()
{
	if (Portal->ActorHasTag(FName("PortalFrozen")) /*Portal->GetActorLabel() == "PortalFrozen"*/)
	{
		//Portal->SetActorLabel("PortalMoving");
		Portal->Tags.Add(FName("PortalMoving"));
		
		//GEngine->AddOnScreenDebugMessage(2, 5, FColor::Red, "PORTAL MOVING");
	}
}



void ASnakeBase1st::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	
}



void ASnakeBase1st::SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 Index;
		SnakeElementArray.Find(OverlappedElement, Index);
		bool bisFirst = Index == 0;

		ISnakeInterface* SnakeInterface = Cast<ISnakeInterface>(Other);
		if (SnakeInterface)
		{
			SnakeInterface->SnakeInteraction(this, bisFirst);
		}
	}
}



void ASnakeBase1st::FoodInteraction(AActor* Interactor)
{
	
}


//void ASnakeBase1st::FoodDeathOwnerElementOverlap(AFoodDeath* OverlappedFoodDeath, AActor* OverlappedFood)
//{
//	if (IsValid(OverlappedFoodDeath))
//	{
//		ISnakeInterface* SnakeInterface = Cast<ISnakeInterface>(OverlappedFood);
//		if (SnakeInterface)
//		{
//			SnakeInterface->FoodInteraction(this);
//		}
//	}
//}

