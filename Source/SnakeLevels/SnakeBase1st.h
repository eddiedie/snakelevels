// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "SnakeInterface.h"
#include "GameFramework/Actor.h"
#include "SnakeBase1st.generated.h"

UENUM(BlueprintType)
//namespace FoodSpace
//{
	enum class EFoodMoving : uint8
	{
		FoodUP = 0 UMETA(DisplayName = "UP"),
		FoodDOWN = 1 UMETA(DisplayName = "DOWN"),
		FoodLEFT = 2 UMETA(DisplayName = "LEFT"),
		FoodRIGHT = 3 UMETA(DisplayName = "RIGHT")
	};
//}

UENUM()
enum class MoveDirection { UP, DOWN, LEFT, RIGHT };

class ASnakeElement;
class AFood;
class AFoodDeath;
class AWallDeath;
class AWallPortal;
class AWalker;
class APortal;



UCLASS()
class SNAKELEVELS_API ASnakeBase1st : public AActor, public ISnakeInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase1st();


	//UPROPERTY(EditDefaultsOnly, Category = "Camera")		int CamPos1st = 1000;
	//UPROPERTY(EditDefaultsOnly, Category = "Camera")		int CamPos2nd = 1000;
	
	

	/*-------------------Snake Element-------------------*/
	UPROPERTY()						ASnakeElement* SnakeElement;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<ASnakeElement> SnakeElementClass;
	UPROPERTY()						TArray<ASnakeElement*> SnakeElementArray;

	//UPROPERTY(EditDefaultsOnly, Category = "Snake")			int		SnakeLives = 4;
	UPROPERTY(EditDefaultsOnly, Category = "Snake")			int		SnakeSize = 50;
	UPROPERTY(EditDefaultsOnly, Category = "Snake")			int		SnakeNum = 5;
	UPROPERTY(EditDefaultsOnly, Category = "Snake")			float	SnakeSpeed = 0.5;
	UPROPERTY(EditDefaultsOnly, Category = "Snake")			float	SpeedUP = 0.25;
	UPROPERTY()												bool	SpeedChange = false;
	
	UPROPERTY(EditDefaultsOnly, Category = "Snake")			MoveDirection LastMoveDirection = MoveDirection::UP;


	/*----------------------------Food------------------------------*/
	UPROPERTY(BlueprintReadWrite)		AFood*					Food;
	UPROPERTY(EditDefaultsOnly)			TSubclassOf<AFood>		FoodClass;
	UPROPERTY()							TArray<AFood*>			FoodArray;
	

	UPROPERTY(/*EditDefaultsOnly, Category = "Food"*/)		int		FoodSize = SnakeSize;
	UPROPERTY(EditDefaultsOnly, Category = "Food")			int		FoodNum = 10;
	UPROPERTY(EditDefaultsOnly, Category = "Food")			int		FoodXRange = 100;
	UPROPERTY(EditDefaultsOnly, Category = "Food")			int		FoodYRange = 100;
	UPROPERTY(EditDefaultsOnly, Category = "Food")			int		FoodRed = 2;
	UPROPERTY(EditDefaultsOnly, Category = "Food")			int		FoodDeathsLeftNum = 2; 
	UPROPERTY(EditDefaultsOnly, Category = "Food")			bool	FoodDelaySetUp;
	UPROPERTY(EditDefaultsOnly, Category = "Food")			int32	FoodMoveDelay = 3;

	UPROPERTY()			EFoodMoving FoodMoving;
	



	/*----------------------------Food Death Wall------------------------------*/
	UPROPERTY(BlueprintReadWrite)		AFoodDeath*					FoodDeath;
	UPROPERTY(EditDefaultsOnly)			TSubclassOf<AFoodDeath>		FoodDeathClass;
	UPROPERTY()							TArray<AFoodDeath*>			FoodDeathArray;

	UPROPERTY(EditDefaultsOnly, Category = "FoodDeathWall")	int32		FoodDeathWallNum = 4;
	UPROPERTY(EditDefaultsOnly, Category = "FoodDeathWall")	bool		FoodDeathDelaySetUp;
	UPROPERTY(EditDefaultsOnly, Category = "FoodDeathWall")	int32		FoodDeathMoveDelay;
	


	/*----------------------------Wall Death----------------------------*/
	UPROPERTY(BlueprintReadWrite)	AWallDeath*					WallDeath;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<AWallDeath>		WallDeathClass;
	UPROPERTY()						TArray<AWallDeath*>			WallDeathArray;




	/*-----------------------------WallPortal-------------------------------*/
	UPROPERTY(BlueprintReadWrite)	AWallPortal*				WallPortal;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<AWallPortal>	WallPortalClass;
	UPROPERTY()						TArray<AWallPortal*>		WallPortalArray;




	/*------------------------------Walker--------------------------------------*/
	UPROPERTY(BlueprintReadWrite)						AWalker*				Walker;
	UPROPERTY(EditDefaultsOnly)							TSubclassOf<AWalker>	WalkerClass;
	UPROPERTY()											TArray<AWalker*>		WalkerArray;

	UPROPERTY(EditDefaultsOnly, Category = "Walking Wall")	int32					WalkerNum = 4;
	UPROPERTY(EditDefaultsOnly, Category = "Walking Wall")	bool					WalkerDelaySetUp;
	UPROPERTY(EditDefaultsOnly, Category = "Walking Wall")	int32					WalkerMoveDelay = 3;
	



	/*------------------------------Portal--------------------------------------*/
	UPROPERTY(BlueprintReadWrite)	APortal*							Portal;
	UPROPERTY(EditDefaultsOnly)		TSubclassOf<APortal>				PortalClass;
	UPROPERTY()						TArray<APortal*>					PortalArray;

	
	//UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Portal Spawning")		float	SpawnDelayMin = 1;
	//UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Portal Spawning")		float	SpawnDelayMax = 5;

	//float SpawnDelay = FMath::FRandRange(SpawnDelayMin, SpawnDelayMax);;

	UPROPERTY(EditDefaultsOnly, Category = "Walking Portal")		int32		PortalNum = 4;
	UPROPERTY(EditDefaultsOnly, Category = "Walking Portal")		bool		PortalDelaySetUp;
	UPROPERTY(EditDefaultsOnly, Category = "Walking Portal")		int32		PortalMoveDelay = 3;
	
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



	

	/*------------------------ Create Snake ------------------------*/
	UFUNCTION(BlueprintCallable)
		void CreateSnakeArray(int SnakeNumber);
	UFUNCTION(BlueprintCallable)
		void SnakeMove();
		void GrowSnakeOnFood();
		UFUNCTION()
	void SetSnakeLives(int32 SetLives);
		



	
	/*------------------------ Create Food ------------------------*/
	void CreateFoodArray(int XRange, int YRange, int FSize);
	void DestroyFoodIntersections();
	void SetupNumberFood(int NumberOfFood);
	void SetFoodCount();
	void SetFoodToRed(int FRed, int FCount);
	

	void FoodMove();
	void FoodMovePlay();
	
	void RemoveDestroyed(AFood* ToRemove);
	void SetFoodDeaths(int32 SetFDeaths);
	void CreateFoodOnPortal();
	

	

	/*------------------------- Create FoodDeath Wall---------------------------*/
	void CreateFoodDeathWall(int FDNum);
	void FoodDeathMove();
	void FoodDeathMovePlay();


	
	
	/*------------------------ Create WallDeath ------------------------*/
	void CreateWall(int XRange, int YRange, int WSize);


	

	
	/*------------------------- Create Walker ---------------------------*/
	void CreateWalker(int Num);
	void WalkerMove();
	void WalkerMovePlay();



	
	/*------------------------- Create Portal ---------------------------*/
	void CreatePortal(int Num);
	void PortalMove();
	void PortalMovePlay();
	void SetRenamePortal(int32 SnakeTailLength);
	void RenamePortal();




	void SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other);
	
	virtual void SnakeInteraction(AActor* Interactor, bool bIsHead) override;



	//void FoodDeathOwnerElementOverlap(AFoodDeath* OverlappedFoodDeath, AActor* OverlappedFood);

	virtual void FoodInteraction(AActor* Interactor) override;
	
};
