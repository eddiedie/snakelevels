// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElement.h"

#include "MyGameInstance.h"
#include "SnakeBase1st.h"
#include "SnakeInterface.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeElement::ASnakeElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SnakeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SnakeMeshComponent"));
	RootComponent = SnakeMeshComponent;
	

}

// Called when the game starts or when spawned
void ASnakeElement::BeginPlay()
{
	Super::BeginPlay();

	SnakeMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElement::HandleBeginOverlap);
	SnakeMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SnakeMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	
}

// Called every frame
void ASnakeElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElement::SetHead_Implementation()
{
	
}

void ASnakeElement::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase1st* SnakeBase1st = Cast<ASnakeBase1st>(Interactor);
		if (IsValid(SnakeBase1st))
		{
			SnakeBase1st->Destroy();
			
			UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
			if (IsValid(MyGameInstance))
			{
				int32 LivesDown = MyGameInstance->Lives--;
				MyGameInstance->UpdateLives();
				MyGameInstance->ShowGameOverMenu();
			}
		}
	}
}

void ASnakeElement::HandleBeginOverlap
(
	UPrimitiveComponent*	OverlappedComponent,
	AActor*					OtherActor,
	UPrimitiveComponent*	OtherComponent,
	int32					OtherBodyIndex,
	bool					bFromSweep,
	const FHitResult&		SweepResult
)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}


void ASnakeElement::ToggleCollision()
{
	if (SnakeMeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		SnakeMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		SnakeMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}