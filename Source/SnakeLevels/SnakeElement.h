// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SnakeInterface.h"
#include "GameFramework/Actor.h"
#include "SnakeElement.generated.h"

class UStaticMeshComponent;
class ASnakeBase1st;

UCLASS()
class SNAKELEVELS_API ASnakeElement : public AActor, public ISnakeInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElement();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
		UStaticMeshComponent* SnakeMeshComponent;

	UPROPERTY()
		ASnakeBase1st* SnakeOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetHead();
		void SetHead_Implementation();

		virtual void SnakeInteraction(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
		void HandleBeginOverlap
			(
				UPrimitiveComponent*		OverlappedComponent,
				AActor*						OtherActor,
				UPrimitiveComponent*		OtherComponent,
				int32						OtherBodyIndex,
				bool						bFromSweep,
				const FHitResult&			SweepResult
			);

		void ToggleCollision();

};
