// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeLevelsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKELEVELS_API ASnakeLevelsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
