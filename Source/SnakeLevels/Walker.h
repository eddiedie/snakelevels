// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SnakeInterface.h"
#include "GameFramework/Actor.h"
#include "Walker.generated.h"

class ASnakeBase1st;

UCLASS()
class SNAKELEVELS_API AWalker : public AActor, public ISnakeInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWalker();

	UPROPERTY()
		ASnakeBase1st* WalkerOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void SnakeInteraction(AActor* Interactor, bool bIsHead) override;

};
