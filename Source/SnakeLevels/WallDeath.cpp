// Fill out your copyright notice in the Description page of Project Settings.


#include "WallDeath.h"

#include "MyGameInstance.h"
#include "SnakeInterface.h"
#include "SnakeBase1st.h"

// Sets default values
AWallDeath::AWallDeath()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWallDeath::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallDeath::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallDeath::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		ASnakeBase1st* SnakeBase1st = Cast<ASnakeBase1st>(Interactor);
		if (IsValid(SnakeBase1st))
		{
			SnakeBase1st->Destroy();

			UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
			if (IsValid(MyGameInstance))
			{
				int32 LivesDown = MyGameInstance->Lives--;
				//MyGameInstance->UpdateLives();
				MyGameInstance->ShowGameOverMenu();
			}
		}
	}
}
