// Fill out your copyright notice in the Description page of Project Settings.


#include "WallPortal.h"
#include "SnakeInterface.h"
#include "SnakeBase1st.h"
#include "SnakeElement.h"
#include "Components/BoxComponent.h"

// Sets default values
AWallPortal::AWallPortal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	
	
}

// Called when the game starts or when spawned
void AWallPortal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallPortal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void AWallPortal::SnakeInteraction(AActor* Interactor, bool bIsHead)
{
	if (Interactor && Interactor != this)
	{
		if (bIsHead)
		{
			auto Snake = Cast<ASnakeBase1st>(Interactor);
			if (IsValid(Snake))
			{
				auto WPLocation = this->GetActorLocation();
				//auto WPLabel = this->GetActorLabel();

				//GEngine->AddOnScreenDebugMessage(1, 1, FColor::Black, FString::Printf(TEXT("WPLocation: %s"), *WPLocation.ToString()));
				//GEngine->AddOnScreenDebugMessage(1, 1, FColor::Black, FString::Printf(TEXT("WPLocation: %s"), *WPLabel));
				//GEngine->AddOnScreenDebugMessage(2, 1, FColor::Blue, WPLabel);



				auto SnakeHead = Snake->SnakeElementArray[0];
			
				if (this->ActorHasTag(FName("WPBot"))/*WPLocation.X == -Snake->FoodXRange/*this && WPLabel == "WPBot"*/)
				{
					SnakeHead->SetActorLocation(FVector(Snake->FoodXRange, 0, 0));
				}
				else if (this->ActorHasTag(FName("WPTop"))/*WPLocation.X == Snake->FoodXRange /*this && WPLabel == "WPTop"*/)
				{
					SnakeHead->SetActorLocation(FVector(-Snake->FoodXRange, 0, 0));
				}
				else if (this->ActorHasTag(FName("WPLeft"))/*WPLocation.Y == -Snake->FoodYRange /*this && WPLabel == "WPLeft"*/)
				{
					SnakeHead->SetActorLocation(FVector(0, Snake->FoodYRange, 0));
				}
				else if (this->ActorHasTag(FName("WPRight"))/*WPLocation.Y == Snake->FoodYRange /*this && WPLabel == "WPRight"*/)
				{
					SnakeHead->SetActorLocation(FVector(0, -Snake->FoodYRange, 0));
				}
			}
		}
	}
}
