// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SnakeInterface.h"
#include "Engine/TriggerBox.h"
#include "GameFramework/Actor.h"
#include "WallPortal.generated.h"


class ASnakeBase1st;
class UBoxComponent;



UCLASS()



class SNAKELEVELS_API AWallPortal : public AActor, public ISnakeInterface//, public ATriggerBox
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallPortal();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SphereComponent")
		UBoxComponent* BoxComponent;
	
	UPROPERTY()
		ASnakeBase1st* WallPortalOwner;

	
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	virtual void SnakeInteraction(AActor* Interactor, bool bIsHead) override;

	

};
