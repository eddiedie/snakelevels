// Fill out your copyright notice in the Description page of Project Settings.


#include "WinMenu.h"
#include "Components/Button.h"
#include "MyGameInstance.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

bool UWinMenu::Initialize()
{
	Super::Initialize();
	UpdateTextScore();
	BtnNextGame->OnClicked.AddDynamic(this, &UWinMenu::StartAgainClicked);
	BtnQuitToMain->OnClicked.AddDynamic(this, &UWinMenu::QuitToMainClicked);

	return true;
}

void UWinMenu::StartAgainClicked()
{
	UE_LOG(LogTemp, Warning, TEXT("Our button is working!"));

	/*Get reference to PlayerController*/
	APlayerController* PLayerController = GetWorld()->GetGameInstance()->GetFirstLocalPlayerController();

	/*Set Up input parameters to the SetInputMode function*/
	FInputModeGameOnly InputModeData;
	//InputModeData.SetWidgetToFocus(MainMenu->TakeWidget());
	//InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	/*Set Input Mode*/
	PLayerController->SetInputMode(InputModeData);
	PLayerController->bShowMouseCursor = false;
	PLayerController->SetPause(false);
	this->RemoveFromParent();


	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		
		if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level1st"))
		{
			UGameplayStatics::OpenLevel(GetWorld(), "Level2nd");
			MyGameInstance->Lives = MyGameInstance->StartLives;
		}
		if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level2nd"))
		{
			UGameplayStatics::OpenLevel(GetWorld(), "Level3rd");
			MyGameInstance->Lives = MyGameInstance->StartLives;
		}
		if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level3rd"))
		{
			UGameplayStatics::OpenLevel(GetWorld(), "Level4th");
			MyGameInstance->Lives = MyGameInstance->StartLives;
		}
		if (UGameplayStatics::GetCurrentLevelName(GetWorld(), true).Equals("Level4th"))
		{
			UGameplayStatics::OpenLevel(GetWorld(), "LevelMainMenu");
			MyGameInstance->Lives = MyGameInstance->StartLives;
			
		}
	}

}



void UWinMenu::QuitToMainClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "LevelMainMenu");
}


void UWinMenu::HideNextGameBtn()
{
	BtnNextGame->RemoveFromParent();
}


void UWinMenu::UpdateTextScore()
{
	UMyGameInstance* MyGameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	if (IsValid(MyGameInstance))
	{
		CountScore = MyGameInstance->Hits;

		TextScoreCounter->SetText(FText::FromString(FString::FromInt(CountScore)));
		//GEngine->AddOnScreenDebugMessage(1, 5, FColor::Cyan, FString::FromInt(MyGameInstance->Hits));
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("Count Hits: %d"), MyGameInstance->Hits));
	}
}
