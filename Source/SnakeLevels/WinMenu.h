// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WinMenu.generated.h"


class UTextBlock;
class UButton;


UCLASS()
class SNAKELEVELS_API UWinMenu : public UUserWidget
{
	GENERATED_BODY()

		virtual bool Initialize();

public:

	UPROPERTY(meta = (BindWidget))
		UButton* BtnNextGame;
	UPROPERTY(meta = (BindWidget))
		UButton* BtnQuitToMain;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextScoreCounter;

	int32 CountScore = 0;

public:
	UFUNCTION()
		void StartAgainClicked();
	UFUNCTION()
		void QuitToMainClicked();
	UFUNCTION()
		void HideNextGameBtn();

	UFUNCTION()
		void UpdateTextScore();
	
};
